# HACIT

Automotive framework connects devices that interface with existing CANBUS infrastructure to assist drivers, informing them of other vehicles while providing infotainment dashboard video and controls. 


# 1. Components
## a. Software
### i. AOSP
### ii. Android Auto HAL
### iii. Infotainment Hub
### iv. External Vehicle Alert Service
### iv. Others TBD
## b. Hardware
### i. riscv
### ii. TI TCAN1043HG-Q1
### iii. GPS
### iv. etc.


# 2. References
## https://www.xda-developers.com/risc-v-cores-and-why-they-matter/

# 3. Development
## a. Set Up developement Environment
### i. Risc-V Emulation:
#### * https://wiki.qemu.org/Documentation/Platforms/RISCV
#### * https://wiki.qemu.org/Documentation
#### * https://fedoraproject.org/wiki/Architectures/RISC-V
#### * https://fedorapeople.org/groups/risc-v/disk-images/readme.txt
#### * https://github.com/riscv/riscv-qemu





















Credits:
Avatar by Simon Claessen
